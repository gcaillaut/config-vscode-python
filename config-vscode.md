# Mise en place d'un environnement de travail Python sous VS Code

## Lancement de Visual Studio Code

Visual Studio Code est un éditeur de texte orienté dossier,c’est à dire qu’il est conçu de sorte à travailler sur des dossiers. Un dossier est considéré comme un projet à part entière.

La commande suivante permet d'ouvrir un dossier, donc un projet, depuis la ligne de commande.

```shell
code mon_dossier
```

Par exemple `code .` permet d'ouvrir le dossier courant.

Il est toutefois possible d’ouvrir un unique fichier, en remplaçant le nom du dossier par un nom de fichier.

```shell
code mon_fichier.txt
```

## Paramétrage de Visual Studio Code

Les paramètres de l'éditeurs sont localisés dans plusieurs fichiers au format JSON. Les paramètres utilisateur s'appliquent sur toutes les instances de Visual Studio Code. La configuration utilisateur peut être modifiée localement par une configuration "Espace de travail".

Les deux fichiers de configuration sont accessibles, et éditables, depuis le menu `Fichier > Préférences > Paramètres` ou le raccourci clavier `Ctrl + ,`. Les paramètre globaux sont localisés dans un fichier dans le répertoire personnel de l'utilisateur tandis que les paramètres locaux sont enregistrés dans le fichier `.vscode/settings.json` du projet courant.

![Configuration de Visual Studio Code](ressources/configuration.jpg)

Par défaut, les paramètres sont modifiables depuis un menu. On peut basculer vers l'affichage brut, en JSON, et éditer manuellement la configuration en cliquant sur le bouton en haut à droite de l'interface de configuration.

Visual Studio Code peut être configuré de nombreuses façon, libre à vous de la paramétrer comme bon vous semble. Toutefois, je vous encourage à affecter la valeur `"onFocusChange"` paramètre `"files.autoSave"`. Cela permet d'enregistrer un document dès que celui-ci perd le focus. Si Visual Studio Code est n'est pas lancé depuis un terminal dans lequel la variable `http_proxy` est correctement configurée, par exemple depuis le lanceur d'applications, alors le proxy doit être paramétré dans Visual Studio Code.

```json
{
    "files.autoSave": "onFocusChange",
    "http.proxy": "http://wwwcache.iut45.univ-orleans.fr:3128",
}
```

## Ajout du support de Python

Par défaut, VS Code ne propose que la coloration syntaxique d'un code Python. Cependant, de nombreuses extensions permettent d'ajouter de nouvelles capacités à l'éditeur.

Les extensions s’installent depuis le menu des extensions sur la gauche de la fenêtre. 

![Menu des extensions](ressources/menu-extensions.jpg)

Cliquer sur une extension de la liste permet d'afficher un bref aperçu des fonctionnalités proposées par l'extension.
Ce menu comporte une barre de recherche ainsi qu'une liste déroulante. Pour installer une extension, il suffit de tapper les mots clés correspondants à l'extension recherchée, puis de l'installer via le bouton prévu à cet effet. 

![Installation d'une extension](ressources/installer-extension.jpg)

Les extensions qui nous intéressent sont les suivantes :

- [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
- [Django](https://marketplace.visualstudio.com/items?itemName=batisteo.vscode-django)
- [Jinja](https://marketplace.visualstudio.com/items?itemName=wholroyd.jinja)

La méta-application [Python Extension Pack](https://marketplace.visualstudio.com/items?itemName=donjayamanne.python-extension-pack) regroupe ces trois extensions ainsi que quelques autres.

## Paramétrage de l'extension Python

L'interpréteur Python par défaut sous ubuntu est Python 2.7. C'est donc cette version qui sera utilisé par L'extension Python. Pour modifier ce comportement, il faut éditer le paramètre `pythonPath` de l'extension Python. Au cours de ce module, vous serez amenés à créer des environnements Python virtuels, les *virtualenvs*. L'idéal est de les créer dans un unique répertoires, par exemple `~/.venvs`. L'extension Python peut alors être paramétrée de sorte à découvrir tous les evironnements virtuels créés dans ce répertoire. Pour ce faire, il faut modifier la valeur de la variable `python.venvPath`.

```json
{
    "python.pythonPath": "python3",
    "python.venvPath": "~/.venvs"
}
```

## Installation des paquets Python requis

L'extension Python fait appel à plusieurs modules Python. Par exemple, le code peut être mis en forme automatiquement via les fonction du module autopep8. Ces modules peuvent être installés depuis le gestionnaire de modules `pip`.

```shell
pip3 install nom-du-module
```

Par exemple, pour activer la mise en forme automatique et afficher les erreurs potentielles d'un code, les extension autopep8 et pylint sont requises.

```shell
pip3 install autopep8
pip3 install pylint
```

Ces modules *devraient* déjà être installé sur les machines de l'IUT. Ils devront toutefois être réinstallés dans chaque environnement virtuel que vous utiliserez.

## Utilisation de l'éditeur

### Débogage

L'extension Python permet de déboguer les programmes Python. Fini les `print` dans le code pour vérifier les valeurs des variables ! L'interface du débogueur est accessible depuis l'icône en forme d'insecte sur la barre de menu sur la droite de l'éditeur.

![Débogueur de Visual Studio Code][ressources/debogage.jpg]

Ce débogueur permet de visualiser de placer des *breakpoints* en cliquant sur les numéros de lignes. Une fois lancé via l'interface de débogage, l'exécution du programme se stoppera au prochain *breakpoint* atteint et l'état du programme sera affiché sur le menu de gauche.

### Tests

L'extension Python permet d'automatiser l'exécution des tests unitaires. Pour activer cette fonctionnalité, il est nécessaire d'installer au préalable le module Python nosetests.

```shell
pip3 install nosetests
```

Ensuite, puisque c'est Python 2.7 qui est utilisé par défaut sous Ubuntu, il est nécessaire de configurer l'extension pour utiliser la version Python 3 de nosetests.

```json
{
    "python.testing.nosetestPath": "nosetests3",
    "python.testing.nosetestsEnabled": false
}
```

L'ajout de ces paramètre aura pour effet d'ajouter une nouvelle icône dans la barre d'outils en bas de la fenêtre.

![Icône des tests unitaires](ressources/run-tests.jpg)

Une fois les tests exécutés, l'icône changera afin d'indiquer le nombre de tests échoués et réussis. Ici, un test a échoué et un autre est passé.

![Icône des tests unitaires après exécution](ressources/etat-tests.jpg)
